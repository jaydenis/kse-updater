@echo off

rem This script cleans up files from the build process
rem and cleans up any binaries compiled.

cd "KSE Updater"
if exist bin rmdir /S /Q bin
if exist obj rmdir /S /Q obj

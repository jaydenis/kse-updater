﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace KSE_Updater
{
	public class Logger
	{
		private string LoggerFile = @".\updater.log";
		private static Logger m_Instance;
		private string logString;
		private string StartTime;

		public static Logger getInstance()
		{
			if (m_Instance == null)
				m_Instance = new Logger();

			return m_Instance;
		}

		private Logger()
		{
			logString = "";
			StartTime = DateTime.Now.ToString();
		}

		public void WriteLog(string text)
		{
			logString += DateTime.Now + ": " + text + "\r\n";
		}

		public void WriteLogToFile()
		{
			if (File.Exists(LoggerFile))
				File.Delete(LoggerFile);

			FileStream file = File.Open(LoggerFile, FileMode.OpenOrCreate, FileAccess.Write);
			StreamWriter writer = new StreamWriter(file);
			writer.WriteLine("=== Begin update log. Start time: " + StartTime + " ===\r\n");
			writer.WriteLine(logString);
			writer.WriteLine("=== End Log. Time: " + DateTime.Now + " ===");
			writer.Flush();
			writer.Close();
		}
	}
}

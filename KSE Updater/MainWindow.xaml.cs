﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.IO;
using System.Diagnostics;
using System.IO.Compression;

namespace KSE_Updater
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private string newProcessName;
		private Logger logger;
		private string UpdateZip = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\kseupdate.zip";
		private string parentProcessName;
		private int parentProcessId;

		public MainWindow(Logger logger)
		{
			InitializeComponent();

			this.logger = logger;
			parentProcessName = App.ParentProcess.ProcessName;
			parentProcessId = App.ParentProcess.Id;

			logger.WriteLog("Initializing download procedure");
			int version;
			string[] pNameParts = parentProcessName.Split('_');
			try
			{
				version = int.Parse(pNameParts[1]);
				newProcessName = string.Format("{0}_{1}.exe", pNameParts[0], App.Version);
			}
			catch
			{
				version = int.Parse(App.Version);
				newProcessName = string.Format("{0}_{1}.exe", "KSE", App.Version);
			}
			//newProcessName = string.Format("{0}_{1}.exe", pNameParts[0], (version + 1));
			

			logger.WriteLog("Starting download");
			DownloadUpdate();
		}

		#region delegates
		private delegate void d_SetProgressMax(int max);
		private delegate void d_SetProgressValue(int value);
		private delegate void d_CloseWindow();

		// Set progress bar maximum
		private void SetProgressMax(int max)
		{
			if (!Dispatcher.CheckAccess())
				Dispatcher.Invoke(new d_SetProgressMax(SetProgressMax), max);
			else
				pbProgress.Maximum = max;
		}

		// Set progress bar current value
		private void SetProgressValue(int value)
		{
			if (!Dispatcher.CheckAccess())
				Dispatcher.Invoke(new d_SetProgressValue(SetProgressValue), value);
			else
				pbProgress.Value = value;
		}

		// Close the window when done
		private void CloseWindow()
		{
			if (!Dispatcher.CheckAccess())
				Dispatcher.Invoke(new d_CloseWindow(CloseWindow));
			else
				App.Current.Shutdown();
		}
		#endregion

		private void DownloadUpdate()
		{
			try
			{
				Process p = Process.GetProcessById(App.ParentProcess.Id);
				p.Kill();
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}

			WebClient client = new WebClient();
			client.DownloadProgressChanged += client_DownloadProgressChanged;
			client.DownloadFileCompleted += client_DownloadFileCompleted;
			
			// Download update file asyncronously. Requires a new thread
			// The reason for the delegates
			client.DownloadFileAsync(new Uri(App.DownloadURL), UpdateZip);
		}

		void client_DownloadFileCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
		{
			// Attempt to kill parent process if it failed to kill earlier
			logger.WriteLog("Download has completed");
			logger.WriteLog("Extracting update");
			lblAction.Text = "Extracting Update...";

			pbProgress.Value = 0;
			pbProgress.IsIndeterminate = true;

			using (ZipArchive archive = ZipFile.OpenRead(UpdateZip))
			{
				foreach (ZipArchiveEntry entry in archive.Entries)
				{
					try
					{
						entry.ExtractToFile(System.IO.Path.Combine(Environment.CurrentDirectory, entry.FullName), true);
					}
					catch
					{
						
					}
				}
			}

			try
			{
				lblAction.Text = "Attempting Process Termination";
				Process p = Process.GetProcessById(App.ParentProcess.Id);
				p.Kill();
				logger.WriteLog("Parent application terminated");
			}
			catch { } // No need to catch anything really.

			CloseWindow();
		}

		void client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
		{
			// Math shit
			double bytesIn = double.Parse(e.BytesReceived.ToString());
			double totalBytes = double.Parse(e.TotalBytesToReceive.ToString());
			SetProgressMax(100);
			double percent = bytesIn / totalBytes * 100;
			SetProgressValue(int.Parse(Math.Truncate(percent).ToString()));
		}

		private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// Attempt to kill process if it still lurks around
			try
			{
				Process p = Process.GetProcessById(App.ParentProcess.Id);
				p.Kill();
				logger.WriteLog("Parent application terminated");
			}
			catch { }

			logger.WriteLog("Cleaning up trace update files");
			if (File.Exists(UpdateZip))
				File.Delete(UpdateZip);

			// Start a new KSE process, but pass in 2 arguments for deleting old version
			// The /DeleteOldVersion flag followed by the file to delete
			ProcessStartInfo psi = new ProcessStartInfo();
			
			if(File.Exists(newProcessName))
			{
				psi.FileName = newProcessName;
				psi.Arguments = "/DeleteOldVersion " + App.ParentProcess.ProcessName + ".exe";
			}
			else
			{
				psi.FileName = "u_" + parentProcessName + ".exe";
			}
				

			// Start the process
			logger.WriteLog("Launching new KSE update");
			Process.Start(psi);

			logger.WriteLog("Update is fully complete, terminating updater");
			logger.WriteLogToFile();
		}
	}
}

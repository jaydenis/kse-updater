﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net;
using System.IO;
using System.Threading.Tasks;

namespace KSE_Updater
{
	/// <summary>
	/// Interaction logic for ChangesWindow.xaml
	/// </summary>
	public partial class ChangesWindow : Window
	{
		private Logger logger;
		public ChangesWindow(Logger logger)
		{
			InitializeComponent();

			Task.Factory.StartNew(() => { LoadChanges(); });
			this.logger = logger;

			logger.WriteLog("Launching changes window");
		}

		private void LoadChanges()
		{
			logger.WriteLog("Attempting to get the changes from the ChangesURL");
			try
			{
				WebRequest req = WebRequest.Create(App.ChangesURL);
				WebResponse res = req.GetResponse();
				using (var reader = new StreamReader(res.GetResponseStream()))
				{
					logger.WriteLog("Changes obtained. Writing them to the window.");
					string line, resp = "";
					while ((line = reader.ReadLine()) != null)
					{
						resp += line + "\r\n";
					}
					SetChangesText(resp);
				}
			}
			catch (Exception ex)
			{
				logger.WriteLog("Error getting changes: " + ex);
				SetChangesText("Error: " + ex);
			}
		}

		private delegate void d_SetChangesText(string text);
		private void SetChangesText(string text)
		{
			if (!Dispatcher.CheckAccess())
				Dispatcher.Invoke(new d_SetChangesText(SetChangesText), text);
			else
				tbChanges.Text = text;
		}

		private void bStart_Click(object sender, RoutedEventArgs e)
		{
			logger.WriteLog("Starting update, launching download window");
			new MainWindow(logger).Show();
			this.Close();
		}

		private void bCancel_Click(object sender, RoutedEventArgs e)
		{
			logger.WriteLog("User decided not to update. Closing updater");
			logger.WriteLogToFile();
			this.Close();
		}
	}
}

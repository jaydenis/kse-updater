﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using System.Diagnostics;
using System.Xml;
using System.Net;
using System.Management;

namespace KSE_Updater
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		public static string Version { get; private set; }
		public static Process ParentProcess { get; private set; }
		public static Dictionary<string, string> arguments = new Dictionary<string, string>();
		public static string DownloadURL { get; private set; }
		public static string ChangesURL { get; private set; }

#if DEBUG
		private static Uri UpdateXMLUrl = new Uri("http://cdn.kalebklein.com/kse/new/debug/kse_update.xml");
#else
		private static Uri UpdateXMLUrl = new Uri("http://cdn.kalebklein.com/kse/new/kse_update.xml");
#endif

		private Logger logger;

		public App()
		{
			// Initialize logger
			logger = Logger.getInstance();

			// Get command line arguments passed in from KSE
			logger.WriteLog("Checking for command line arguments");
			string[] cmdArgs = Environment.GetCommandLineArgs();
			// Sift through all cmd line arguments
			for(int i = 0; i < cmdArgs.Length; i++)
			{
				// Commands start with a / because I said they should
				// Filter them out. Anything that comes after the command will the the
				// value that command should hold
				if(cmdArgs[i].StartsWith("/"))
				{
					logger.WriteLog("Command " + cmdArgs[i] + " found");
					string arg = cmdArgs[i]; // Set argument to the argument supplied
					string val = ""; // Initialize value string
					try
					{
						// Check if subsequent arguments are supplied with the / delemeter
						if (!cmdArgs[i + 1].StartsWith("/"))
						{
							logger.WriteLog("Setting value " + cmdArgs[i + 1] + " to argument " + cmdArgs[i]);
							// Set argument value
							val = cmdArgs[i + 1];
						}
					}
					catch
					{
						val = "";
					}
					// add arguments and values into the dictionary
					arguments.Add(arg, val);
				}
			}

			// Begin the update check
			logger.WriteLog("Starting update checking process");
			CheckForUpdates();
		}

		private void CheckForUpdates()
		{
			// Check if connected to the internet
			StatusCheck sc = new StatusCheck("http://cdn.kalebklein.com/kse/new/testc.txt");
			logger.WriteLog("Checking for connection to update server");
			if (sc.IsConnected)
			{
				logger.WriteLog("Connection established. Obtaining info from server");
				// Load in the XML documnet
				XmlDocument doc = new XmlDocument();
				string xmlStr = "";

				using (var wc = new WebClient())
				{
					// XML document is on the web, download it and place it's contents into a string
					xmlStr = wc.DownloadString(UpdateXMLUrl);
				}
				doc.LoadXml(xmlStr); // Load the XML from the downloaded string

				XmlNode uNode = doc["updater"]; // Get updater node
				Dictionary<string, string> nodeList = new Dictionary<string, string>();
				foreach(XmlNode node in uNode.ChildNodes)
				{
					// Add nodes into dictionary
					if(!node.Name.StartsWith("#comment"))
					{
						nodeList.Add(node.Name, node.InnerText.Trim());
					}
				}

				// Remove version gotten from the XML file
				Version oVersion = new Version(nodeList["version"]);

				// Local version obtained from KSE. Remove the v on the string
				string v = arguments["/Version"].Replace("a", "1")
					.Replace("b", "2")
					.Replace("c", "3")
					.Replace("d", "4")
					.Replace("e", "5")
					.Replace("f", "6")
					.Replace("g", "7")
					.Replace("h", "8")
					.Replace("i", "9")
					.Replace("v", "");

				Version lVersion = new Version(v);
				Version = nodeList["version"].Replace(".", "");

				logger.WriteLog("Checking for new version");
				if(oVersion > lVersion)
				{
					logger.WriteLog("A newer version of KSE has been found.");
					string message = string.Format(@"There is a new update out.

Current Version: {0}
New Version: {1}

Would you like to update now?", arguments["/Version"], nodeList["version"]);

					DownloadURL = nodeList["downloadURL"];
					ChangesURL = nodeList["changesURL"];

					logger.WriteLog(string.Format("Information from server obtained. Information collected is as follows. Download URL: {0} | Changes URL: {1}", DownloadURL, ChangesURL));

					logger.WriteLog("Getting parent process handle");
					ParentProcess = KSEProcess.GetParentProcess();

					new ChangesWindow(logger).Show();
				}
				else
				{
					// If user launches updater manually, /LaunchFromApplication isn't supplied.
					if (!arguments.ContainsKey("/LaunchFromApplication") || (!arguments.ContainsKey("/Version") && !arguments.ContainsKey("/LaunchFromApplication")))
					{
						logger.WriteLog("KSE is currently up to date, nothing else left to do");
						MessageBox.Show("You are currently up to date!", "Check for Updates");
						logger.WriteLogToFile();
						App.Current.Shutdown();
					}
					App.Current.Shutdown();
				}
			}
			else
			{
				if (!arguments.ContainsKey("/LaunchFromApplication") || (!arguments.ContainsKey("/Version") && !arguments.ContainsKey("/LaunchFromApplication")))
				{
					logger.WriteLog("Cannot determine if user is up to date, connection to server failed.\r\n\r\nStatus Code: " + sc.StatusCode);
					MessageBox.Show("A connection could not be established to the server.", "Updater");
					logger.WriteLogToFile();
					App.Current.Shutdown();
				}
				App.Current.Shutdown();
			}
		}
	}
}

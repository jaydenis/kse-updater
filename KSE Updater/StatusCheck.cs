﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace KSE_Updater
{
	public partial class StatusCheck
	{
		/// <summary>
		/// Boolean to tell if there's an internet connection or not
		/// </summary>
		public bool IsConnected
		{
			get;
			private set;
		}

		public string StatusCode
		{
			get;
			private set;
		}

		/// <summary>
		/// URL of whatever you wish to test a connection to
		/// Private - Only accessible to this class
		/// </summary>
		private string URL;

		public StatusCheck()
		{
			this.URL = "";
			testConnection();
		}

		public StatusCheck(String url)
		{
			this.URL = url;
			testConnection();
		}

		private void testConnection()
		{
			string url = (this.URL.Length == 0) ? "http://www.google.com" : this.URL;
			//HttpWebRequest hwrq = (HttpWebRequest)HttpWebRequest.Create(url);
			//hwrq.Timeout = 5000;

			//try
			//{
			//	HttpWebResponse hwrp = (HttpWebResponse)hwrq.GetResponse();
			//	if (hwrp.StatusCode == HttpStatusCode.OK)
			//		IsConnected = true;

			//	StatusCode = hwrp.StatusCode.ToString();
			//}
			//catch
			//{
			//	IsConnected = false;
			//}
			WebRequest req = WebRequest.Create(url);
			try
			{
				var res = (HttpWebResponse)req.GetResponse();
				if (res.StatusCode == HttpStatusCode.OK)
					IsConnected = true;
			}
			catch
			{
				IsConnected = false;
			}
		}
	}
}
